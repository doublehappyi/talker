# coding:utf-8
__author__ = 'db'
import os.path
import tornado.web
import tornado.websocket


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class MessageHandler(tornado.websocket.WebSocketHandler):
    clients = []
    count = 0

    def open(self, *args, **kwargs):
        print "open..."
        MessageHandler.count += 1
        MessageHandler.clients.append({
            "name": MessageHandler.count,
            "client": self
        })

        # self.write_message(MessageHandler.count)


    def on_close(self):
        for client in MessageHandler.clients:
            if client["client"] == self:
                MessageHandler.clients.remove(client)
                break

    def on_message(self, message):
        for client in MessageHandler.clients:
            if client["client"] == self:
                name = client["name"]
                break
        MessageHandler.update_clients(message, name)

    @classmethod
    def update_clients(cls, message, name):
        for client in cls.clients:
            client["client"].write_message({
                "name": name,
                "message": message
            })


class PostHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.render("post.html")

    def post(self, *args, **kwargs):
        callback = self.get_argument("callback", "hello")
        ret = """
            <script type="text/javascript">
            try{
                window.top.window.%s({
                    "name":"yisx"
                })
            }catch (e){}
            </script>
        """ % callback

        self.write(ret)


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
HANDLERS = [(r'/', MainHandler), (r'/websocket', MessageHandler), (r'/post', PostHandler)]
SETTINGS = {
    "template_path": os.path.join(BASE_DIR, "web", "templates"),
    "static_path": os.path.join(BASE_DIR, "web", "static"),
    "debug": True
}

app = tornado.web.Application(HANDLERS, **SETTINGS)

if __name__ == "__main__":
    app.listen(8080)
    tornado.ioloop.IOLoop.instance().start()