/**
 * Created by db on 15-5-9.
 */
window.onload = function () {
    window.WebSocket = window.WebSocket || window.MozWebSocket || undefined;
    if (!window.WebSocket) {
        alert("浏览器不支持WebSocket，请使用最新的chrome或者firefox浏览器");
        return
    }
    var host = "ws://127.0.0.1:8080/websocket";
    var w = new window.WebSocket(host);

    var $content = $("#t-content");
    var $message = $("#t-message");
    var $send = $("#t-send");

    $send.click(function () {
        var message = $message.val();
        if (w.readyState == WebSocket.OPEN) {
            w.send(message);
        }
    });

    w.onmessage = function (e) {
        var data = JSON.parse(e.data);
        var html = '<div class="t-item">' +
            '<a href="#" class="t-name">游客' + data.name + '：</a>' +
            '<p class="t-words">' + data.message + '</p>' +
            '</div>';


        $content.append(html);
    };

    w.onopen = function (e) {
        var data = JSON.parse(e.data);

    };
}