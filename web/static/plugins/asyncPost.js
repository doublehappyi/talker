/**
 * Created by db on 5/22/15.
 */
(function () {
    $.fn.asyncPost = function (options) {
        var defaults = {
            callback: function (data) {
                console.log("data: ", data);
            }
        };
        var options = $.extend({}, defaults, options);
        var createIframe = (function () {
            var seed = 0;
            var $body = $('body');
            return function () {
                var id = "iframe" + seed;
                var $iframe = $('<iframe style="display:block" name="' + id + '" id="' + id + '"></iframe>');
                $body.append($iframe);
                seed++;
                return $iframe;
            }
        })();
        $(this).each(function () {
            var $form = $(this);
            var $iframe = createIframe();
            var action = options["action"] || $form.attr("action");
            var iframeName = $iframe.attr("name");
            var callback = "asyncPost" + iframeName;
            window[callback] = options.callback;
            $form.attr("target", $iframe.attr("id")).attr("action", action + "?callback=" + callback);
            $iframe.load(function(){
                //delete window[callback];
            });
        });
    };
})(jQuery);